import socket
import time
import random
# import numpy as np


class UdpServer:
    """
    @:param delay Sleep time in ms
    """
    def __init__(self, ip="192.168.31.60", port=5005, delay=500):
        self.ip = ip
        self.port = port
        self.message = chr(2)+chr(83)+chr(32)+chr(0)+chr(53)+chr(48)+chr(48)+chr(46)+chr(48)+chr(48)+chr(3)
        # 44 == , and 46 == .
        # 78 == N and 83 == S
        self.delay = delay
        self.list_weights = ["3,210", "21.00", "25.25", "51.50", "125.2", "501.5", "985.0", "1020.0"]

    def generate_weight(self):
        # stability = np.random.choice(["S", "N"], p=[0.7, 0.3])
        stability = random.choice(["S", "N"])
        weight = random.choice(self.list_weights)
        return chr(2)+stability+chr(0)+weight+chr(3)

    def run(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

        # Enable broadcasting mode
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

        while True:
            try:
                sock.connect(("8.8.8.8", 80))
                hostname = socket.gethostname()
                local_ip = sock.getsockname()[0]
                print(f'Starting up server on {local_ip}')
                local_ip = local_ip.split(".")
                local_ip[3] = "255"
                server_ip = ".".join(local_ip)

                print(f"Hostname: {hostname}\tBroadcast IP: {server_ip} on port 2222")
                while True:
                    message = self.generate_weight()
                    for _ in range(5):
                        sock.sendto(message.encode(), (server_ip, 2222))
                        time.sleep(self.delay/1000)

            except ConnectionResetError:
                print("[+] Connection finished by client.")
            finally:
                # Clean up the connection
                sock.close()


if __name__ == "__main__":
    udpServer = UdpServer(delay=500)
    udpServer.run()
