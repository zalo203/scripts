import socket
import time

def run_client_tcp():
    TCP_IP = "192.168.31.184"
    TCP_PORT = 5005
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # UDP

    # sock.bind((TCP_IP, TCP_PORT))
    sock.connect((TCP_IP, TCP_PORT))
    while True:
        msg = chr(ord("P"))+chr(13)  # 80->P
        sock.sendall(msg.encode())
        answer = sock.recv(11)
        # data = sock.recv(1024)  # buffer size is 1024 bytes
        print(answer)
        time.sleep(0.5)
