import socket


UDP_IP = "192.168.31.60"
UDP_PORT = 5005
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP

sock.bind((UDP_IP, UDP_PORT))

while True:
    data, _ = sock.recvfrom(1024)  # buffer size is 1024 bytes
    print("received message: %s" % data)
